package dev.chloe.sulfur.mixins;

import dev.chloe.sulfur.api.ILightingEngineProvider;
import dev.chloe.sulfur.mod.world.lighting.LightingEngine;
import net.minecraft.util.BlockPos;
import net.minecraft.world.EnumSkyBlock;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(World.class)
public abstract class MixinWorld implements ILightingEngineProvider {
    @Unique
    private LightingEngine sulfur$lightingEngine;

    /**
     * @author Angeline
     * Initialize the lighting engine on world construction.
     */
    @Inject(method = "<init>", at = @At("RETURN"))
    private void onConstructed(CallbackInfo ci) {
        this.sulfur$lightingEngine = new LightingEngine((World) (Object) this);
    }

    /**
     * Directs the light update to the lighting engine and always returns a success value.
     * @author Angeline
     */
    @Inject(method = "checkLightFor", at = @At("HEAD"), cancellable = true)
    private void checkLightFor(EnumSkyBlock type, BlockPos pos, CallbackInfoReturnable<Boolean> cir) {
        this.sulfur$lightingEngine.scheduleLightUpdate(type, pos);

        cir.setReturnValue(true);
    }

    @NotNull
    @Override
    public LightingEngine sulfur$getLightingEngine() {
        return this.sulfur$lightingEngine;
    }
}
