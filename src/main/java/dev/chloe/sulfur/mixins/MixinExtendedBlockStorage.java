package dev.chloe.sulfur.mixins;

import net.minecraft.world.chunk.NibbleArray;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;

@Mixin(ExtendedBlockStorage.class)
public class MixinExtendedBlockStorage {
    @Shadow
    public NibbleArray skylightArray;

    @Shadow
    public int blockRefCount;

    @Shadow
    public NibbleArray blocklightArray;

    @Unique
    private int sulfur$lightRefCount = -1;

    /**
     * @author Angeline
     * @author Reset lightRefCount on call
     * @reason r
     */
    @Overwrite
    public void setExtSkylightValue(int x, int y, int z, int value) {
        this.skylightArray.set(x, y, z, value);
        this.sulfur$lightRefCount = -1;
    }

    /**
     * @author Angeline
     * @author Reset lightRefCount on call
     * @reason r
     */
    @Overwrite
    public void setExtBlocklightValue(int x, int y, int z, int value) {
        this.blocklightArray.set(x, y, z, value);
        this.sulfur$lightRefCount = -1;
    }

    /**
     * @author Angeline
     * @author Reset lightRefCount on call
     * @reason r
     */
    @Overwrite
    public void setBlocklightArray(NibbleArray array) {
        this.blocklightArray = array;
        this.sulfur$lightRefCount = -1;
    }

    /**
     * @author Angeline
     * @reason Reset lightRefCount on call
     */
    @Overwrite
    public void setSkylightArray(NibbleArray array) {
        this.blocklightArray = array;
        this.sulfur$lightRefCount = -1;
    }


    /**
     * @author Angeline
     * @reason Send light data to clients when lighting is non-trivial
     */
    @Overwrite
    public boolean isEmpty() {
        if (this.blockRefCount != 0) {
            return false;
        }

        // -1 indicates the lightRefCount needs to be re-calculated
        if (this.sulfur$lightRefCount == -1) {
            if (this.sulfur$checkLightArrayEqual(this.skylightArray, (byte) 0xFF)
                    && this.sulfur$checkLightArrayEqual(this.blocklightArray, (byte) 0x00)) {
                this.sulfur$lightRefCount = 0; // Lighting is trivial, don't send to clients
            } else {
                this.sulfur$lightRefCount = 1; // Lighting is not trivial, send to clients
            }
        }

        return this.sulfur$lightRefCount == 0;
    }

    @Unique
    private boolean sulfur$checkLightArrayEqual(NibbleArray storage, byte val) {
        if (storage == null) {
            return true;
        }

        byte[] arr = storage.getData();

        for (byte b : arr) {
            if (b != val) {
                return false;
            }
        }

        return true;
    }
}
