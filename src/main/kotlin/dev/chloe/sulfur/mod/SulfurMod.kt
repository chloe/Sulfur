package dev.chloe.sulfur.mod

import net.weavemc.loader.api.ModInitializer
import org.apache.logging.log4j.LogManager

class SulfurMod : ModInitializer {
    override fun preInit() {
        LOGGER.info("Loaded Sulfur.")
    }

    companion object {
        @JvmField
        val LOGGER = LogManager.getLogger("Sulfur")
    }
}
