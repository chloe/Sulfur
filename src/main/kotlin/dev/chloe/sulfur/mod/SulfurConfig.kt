package dev.chloe.sulfur.mod

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.annotations.SerializedName
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException

object SulfurConfig {
    private val gson = createGson()

    @SerializedName("enable_sulfur")
    var enableSulfur = true
    private fun saveConfig() {
        val dir = configDirectory
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw RuntimeException("Could not create configuration directory at '" + dir.absolutePath + "'")
            }
        } else if (!dir.isDirectory) {
            throw RuntimeException("Configuration directory at '" + dir.absolutePath + "' is not a directory")
        }
        try {
            FileWriter(configFile).use { writer -> gson.toJson(this, writer) }
        } catch (e: IOException) {
            throw RuntimeException("Failed to serialize config to disk", e)
        }
    }

    private val configDirectory: File
        get() = File("weavecfg")
    private val configFile: File
        get() = File(configDirectory, "sulfur.json")

    private fun createGson(): Gson {
        return GsonBuilder().setPrettyPrinting().create()
    }

    init {
        val file = configFile

        if (!file.exists()) {
            saveConfig()
        } else {
            try {
                FileReader(file).use { reader ->
                    JsonParser().parse(reader)?.run {
                        if(!this.isJsonNull && this.isJsonObject) {
                            enableSulfur = (this as JsonObject).get("enable_sulfur")?.asBoolean ?: true
                        }
                    }
                }
            } catch (e: IOException) {
                throw RuntimeException("Failed to deserialize config from disk", e)
            }

        }
    }
}
