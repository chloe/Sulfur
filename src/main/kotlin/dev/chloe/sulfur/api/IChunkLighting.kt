package dev.chloe.sulfur.api

import net.minecraft.util.BlockPos
import net.minecraft.world.EnumSkyBlock

interface IChunkLighting {
    fun getCachedLightFor(enumSkyBlock: EnumSkyBlock?, pos: BlockPos): Int
}
