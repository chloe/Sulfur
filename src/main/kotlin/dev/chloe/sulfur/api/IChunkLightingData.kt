package dev.chloe.sulfur.api

interface IChunkLightingData {
    fun getNeighborLightChecks(): ShortArray?
    fun setNeighborLightChecks(value: ShortArray?)
    fun isLightInitialized(): Boolean
    fun setLightInitialized(value: Boolean)
    fun setSkylightUpdatedPublic()
}
