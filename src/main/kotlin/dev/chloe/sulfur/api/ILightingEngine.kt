package dev.chloe.sulfur.api

import net.minecraft.util.BlockPos
import net.minecraft.world.EnumSkyBlock

interface ILightingEngine {
    fun scheduleLightUpdate(lightType: EnumSkyBlock?, pos: BlockPos?)
    fun processLightUpdates()
    fun processLightUpdatesForType(lightType: EnumSkyBlock?)
}
