package dev.chloe.sulfur.api

interface ILightingEngineProvider {
    fun `sulfur$getLightingEngine`(): ILightingEngine
}
