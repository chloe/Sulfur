# Sulfur

Sulfur is a port of the lighting engine optimization mod Phosphor, for the [Weave Loader](https://github.com/Weave-MC/Weave-Loader).

## License

Sulfur is licensed under GNU GPLv3, a free and open-source license. For more information, please see the [license file](https://codeberg.org/chloe/Sulfur/src/branch/main/LICENSE).

## Credits
- HowardZHY for Phosphor-Legacy-Forge, the mod that I ported
- 1lylily for helping me with mixins